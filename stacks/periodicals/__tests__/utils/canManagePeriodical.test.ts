jest.mock('../../src/services/periodical', () => ({
  fetchPeriodical: jest.fn().mockReturnValue(Promise.resolve({
    creatorSessionId: 'Arbitrary session ID',
  })),
}));
jest.mock('../../../../lib/utils/periodicals', () => ({
  isPeriodicalOwner: jest.fn().mockReturnValue(true),
}));

import { canManagePeriodical } from '../../src/utils/canManagePeriodical';

const mockDatabase: any = {};
const mockSession = {
  account: {
    identifier: 'Arbitrary account ID',
  },
  identifier: 'Arbitrary session ID',
};

it('should error when the given journal could not be found', () => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.fetchPeriodical.mockReturnValueOnce(Promise.reject(new Error('Some error')));

  const promise = canManagePeriodical(mockDatabase, 'arbitrary id', mockSession);

  return expect(promise).rejects.toEqual(new Error('Some error'));
});

it('should return false when a non-public journal is not owned by the current session', () => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.fetchPeriodical.mockReturnValueOnce(Promise.resolve({
    creatorSessionId: 'some session ID',
  }));
  const mockOtherSession = { identifier: 'some other session ID' };

  const promise = canManagePeriodical(mockDatabase, 'arbitrary id', mockOtherSession);

  return expect(promise).resolves.toBe(false);
});

it('should return false when a public journal is not owned by the current account', () => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.fetchPeriodical.mockReturnValueOnce(Promise.resolve({
    creator: { identifier: 'arbitrary account ID' },
  }));
  const mockedPeriodicalUtil = require.requireMock('../../../../lib/utils/periodicals');
  mockedPeriodicalUtil.isPeriodicalOwner.mockReturnValueOnce(false);

  const promise = canManagePeriodical(mockDatabase, 'arbitrary id', mockSession);

  return expect(promise).resolves.toBe(false);
});

it('should return true when a non-public journal is owned by the current session', () => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.fetchPeriodical.mockReturnValueOnce(Promise.resolve({
    creatorSessionId: 'some session ID',
  }));
  const mockOtherSession = { identifier: 'some session ID' };

  const promise = canManagePeriodical(mockDatabase, 'arbitrary id', mockOtherSession);

  return expect(promise).resolves.toBe(true);
});

it('should return true when a public journal is owned by the current account', () => {
  const mockedPeriodicalUtil = require.requireMock('../../../../lib/utils/periodicals');
  mockedPeriodicalUtil.isPeriodicalOwner.mockReturnValueOnce(true);

  const promise = canManagePeriodical(mockDatabase, 'arbitrary id', mockSession);

  return expect(promise).resolves.toBe(true);
});
