jest.mock('../../src/services/periodical', () => ({
  getHeaderUploadUrl: jest.fn().mockReturnValue(Promise.resolve(
    {
      object: {
        toLocation: 'arbitrary_upload_url',
      },
      result: {
        image: 'arbitrary_filename',
      },
    },
  )),
}));
jest.mock('axios');

import {
  HeaderManager,
} from '../../src/components/headerManager/component';

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

it('should display just the upload form if no header image is known yet', () => {
  const component = shallow(<HeaderManager periodicalId="arbitrary_id" file={}/>);

  expect(toJson(component)).toMatchSnapshot();
});

it('should display the filename of selected files', () => {
  const component = shallow(<HeaderManager periodicalId="arbitrary_id" file={}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'some_filename' } ],
    },
  });

  expect(component.find('.file-name').first().text()).toBe('some_filename');
});

it('should not allow uploading when no file to upload has been selected', () => {
  const component = shallow(<HeaderManager periodicalId="arbitrary_id" file={}/>);

  expect(component.find('[type="submit"]').prop('disabled')).toBe(true);
});

it('should not allow uploading when multiple files have been selected to upload', () => {
  const component = shallow(<HeaderManager periodicalId="arbitrary_id" file={}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [
        { name: 'arbitrary_filename' },
        { name: 'arbitrary_other_filename' },
      ],
    },
  });

  expect(component.find('[type="submit"]').prop('disabled')).toBe(true);
});

it('should block uploading when no file has been selected to upload', () => {
  const component = shallow(<HeaderManager periodicalId="arbitrary_id" file={}/>);

  component.find('form').simulate('submit', { preventDefault: jest.fn() });
  expect(component.state('isUploading')).toBe(false);
});

it('should fetch an upload URL after submitting the form with valid values', () => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  const component = shallow(<HeaderManager periodicalId="some_id" file={}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'some_filename' } ],
    },
  });

  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  expect(mockedPeriodicalService.getHeaderUploadUrl.mock.calls.length).toBe(1);
  expect(mockedPeriodicalService.getHeaderUploadUrl.mock.calls[0][0]).toBe('some_id');
  expect(mockedPeriodicalService.getHeaderUploadUrl.mock.calls[0][1]).toBe('some_filename');
});

it('should display an error message when an upload URL could not be obtained', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getHeaderUploadUrl.mockReturnValueOnce(Promise.reject('Arbitrary error'));
  const component = shallow(<HeaderManager periodicalId="some_id" file={}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'some_filename' } ],
    },
  });

  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  setImmediate(() => {
    component.update();
    expect(component.find('.message.is-danger').text())
      .toBe('Something went wrong uploading your image, please try again.');

    done();
  });
});

it('should display an error message if the connection to S3 failed', (done) => {
  const mockedAxios = require.requireMock('axios');
  mockedAxios.put.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const component = shallow(<HeaderManager periodicalId="arbitrary_id" file={}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'arbitrary_filename' } ],
    },
  });

  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  setImmediate(() => {
    component.update();
    expect(component.find('.message.is-danger').text())
      .toBe('Something went wrong uploading your image, please try again.');

    done();
  });
});

it('should call the onUpload callback after upload, if set', (done) => {
  const callback = jest.fn();
  const component = shallow(<HeaderManager periodicalId="arbitrary_id" file={} onUpload={callback}/>);

  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getHeaderUploadUrl.mockReturnValueOnce(Promise.resolve({
    object: {
      toLocation: 'arbitrary_upload_url',
    },
    result: {
      image: 'some_filename',
    },
  }));

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'arbitrary_filename' } ],
    },
  });

  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  setImmediate(() => {
    expect(callback.mock.calls.length).toBe(1);
    expect(callback.mock.calls[0][0]).toEqual('some_filename');

    done();
  });
});

it('should display the uploaded image after upload', (done) => {
  const file = 'some_url';
  const component = shallow(<HeaderManager periodicalId="arbitrary_id" file={file}/>);

  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getHeaderUploadUrl.mockReturnValueOnce(Promise.resolve({
    object: {
      toLocation: 'arbitrary_upload_url',
    },
    result: {
      image: 'some_file_url',
    },
  }));

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'arbitrary_other_filename' } ],
    },
  });

  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  setImmediate(() => {
    component.update();
    expect(component.find('img[src="some_file_url"]')).toExist();

    done();
  });
});
